//
// Created by caskeep on 18-9-6.
//

#include <iostream>
#include <string>
#include <map>


using std::cout;
using std::endl;
using std::ostream;
using std::string;

class Testkey {
    std::string string;
};


class TestData {
public:
    TestData() {
        cout << "TestData 0 param construct." << endl;
    };

    TestData(int val1, const string &val2) : val1(val1), val2(val2) {
        cout << "TestData 2 param construct." << endl;
    }

    TestData(const TestData &data) {
        cout << "TestDat copy construct." << endl;
        val1 = data.val1;
        val2 = data.val2;
    }

    TestData &operator=(const TestData &data) {
        cout << "TestDat copy = construct." << endl;
        val1 = data.val1;
        val2 = data.val2;
        return *this;
    }

//    TestData(TestData &&data) noexcept {
//        cout << "TestData move construct." << endl;
//        val1 = data.val1;
//        val2 = std::move(data.val2);
//    }
//
//    TestData& operator=(TestData &&data) noexcept {
//        cout << "TestData move = construct." << endl;
//        val1 = data.val1;
//        val2 = std::move(data.val2);
//        return *this;
//    }

    ~TestData() { cout << "TestData destruct." << endl; }

    friend ostream &operator<<(ostream &, const TestData &);

private:
    int val1 = 1010;
    string val2 = "abc";
};


ostream &operator<<(ostream &out, const TestData &d) {
    out << "{ " << d.val1 << " and " << d.val2 << " }";
    return out;
}


int
main(int argc, char *argv[]) {
    using TMapTest1 = std::map<int, TestData>;
    cout << "Test1 Start." << endl;
    auto printAllInMap = [](const TMapTest1 &map) {
        for (const auto &k2v : map)
            cout << "key: " << k2v.first << " -> val: " << k2v.second << endl;
    };
    TMapTest1 test1Map;
    cout << "Test1 Add by emplace." << endl;
    test1Map.emplace();
    test1Map.emplace(std::piecewise_construct, std::make_tuple(10), std::make_tuple());
    test1Map.emplace(std::piecewise_construct, std::make_tuple(11), std::make_tuple(2, "zxc"));
    cout << "Test1 Add by emplace end." << endl;
    cout << "Test1 Add by insert." << endl;
    test1Map.insert(std::make_pair(4, TestData(4, "qwe")));
    cout << "Test1 Add by insert end." << endl;
    cout << "Test1 Add by [] operator." << endl;
    test1Map[7] = TestData(8, "rty");
    cout << "Test1 Add by [] operator end." << endl;
    printAllInMap(test1Map);
    cout << "Test1 End." << endl;
    return 0;
}